var fs = require('fs');
var async = require('async');
var oracledb = require('oracledb');
//var dbConfig = require('./dbconfig.js');

var conn;
var sql;
var outFileName;

function con(cb) {
    oracledb.getConnection(
        {
            user: "USRCONSULTA",
            password: "USRCONSULTA",
            connectString: 'rac-test.ipa.derfe.ine.mx:12510/VOLU1'
        },
        function (err, connection) {
            if (err)
                return cb(err);
            else {
                conn = connection;
                return cb(null);
            }
        });
};

var dorelease = function () {
    conn.close(function (err) {
        if (err)
            console.error(err.message);
    });
};

// Stream a LOB to a file
function temp3(lob, cb) {
    console.log("paso 3" + JSON.stringify(lob.body))
    //console.log("antes construir "+JSON.stringify(lob));
    if (lob.type === oracledb.CLOB) {
        console.log('Writing a CLOB to ' + outFileName);
        lob.setEncoding('utf8');  // set the encoding so we get a 'string' not a 'buffer'
    } else {
        console.log('Writing a BLOB to ' + outFileName);
    }

    var errorHandled = false;
    //console.log("des construir "+JSON.stringify(lob));
    lob.on(
        'error',
        function (err) {
            console.log("lob.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(function () {
                    return cb(err);
                });
            }
        });
    lob.on(
        'end',
        function () {
            console.log("lob.on 'end' event");
        });
    lob.on(
        'close',
        function () {
            // console.log("lob.on 'close' event");
            if (!errorHandled) {
                return cb(null);
            }
        });

    var outStream = fs.createWriteStream(outFileName);
    outStream.on(
        'error',
        function (err) {
            console.log("outStream.on 'error' event");
            if (!errorHandled) {
                errorHandled = true;
                lob.close(function () {
                    return cb(err);
                });
            }
        });
    console.log("termina")
    // Switch into flowing mode and push the LOB to the file
    lob.pipe(outStream);
};
