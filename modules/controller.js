const DBManager = require('../DBManager/DBManager');
var blob = require("../modules/blob");
var dbConfig = require('../config/dbconfig');
const request = require("request-promise");
var fs = require('fs');
//const dbConfig = require('../config/dbconfig');

/* 
========================================================

    metodos para el API

========================================================
*/
var connection = {
    // user: dbConfig[0].db.user,
    // password: dbConfig[0].db.password,
    // connectString: dbConfig[0].db.connectString
};

/**
 * 
 * @param {*} fuar 
 * @param {*} foto 
 * @param {*} json 
 * @param {*} callback 
 */
function apiFoto(fuar, base, callback) {
    const firm = "[Controller::apiFoto] ";
    var fecha_inicio = new Date();
    console.log(firm + "inicio " + fecha_inicio + " FUAR [" + fuar + "] base:" + base)
    const json = {};
    var exp = "^([0-9]){13}$";
    var sql = fuar.match(exp) == null ? "select FOTOGRAFIA_ID from NOTIFICACIONESEXTRANJERO.NOTIFICACION where fuar ='" + fuar + "' " :
        "SELECT FOTOGRAFIA_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22) order by  estatusnotificacion_id ";
    //console.log(firm + sql);
    var connection = {
        user: dbConfig.base[parseInt(base)].user,
        password: dbConfig.base[parseInt(base)].password,
        connectString: dbConfig.base[parseInt(base)].connectString
    };
    DBManager.ejecute(sql, connection, function(err, result) {
        if (err) {
            json["foto"] = {
                    code: 400,
                    descripcion: "error conexion base de datos  "
                }
                //console.log(json)
            callback(true, json);
        } else {
            //let foto = result[0][1];

            if (result[0] == undefined) {
                json["foto"] = {
                    code: 401,
                    descripcion: "no existe el fuar "
                }
                console.log(firm + json)
                callback(false, json);
            } else {
                //console.log(firm + result[0])
                if (result[0] == undefined) {
                    // console.log(result);
                    json["foto"] = {
                        code: 402,
                        descripcion: "No existen datos"
                    }
                    var fecha_fin = new Date();
                    //console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio) + " -- falloo");
                    callback(false, json)
                } else {
                    //console.log(firm + "------" + dbConfig.base[parseInt(base)].prefijo)
                    var prefijo = dbConfig.base[parseInt(base)].prefijo ? ",'pu'" : '';
                    //var sql = "select  CEPH.GET_FOTO('" + result[0][0] + "') FROM DUAL";
                    var sql = `select  CEPH.GET_FOTO('${result[0][0]}'${prefijo}) FROM DUAL`;
                    //console.log(firm, "--------------------", prefijo, "++", sql)
                    blob.run(sql, base, "foto", (fuar + ".jpg"), json, function(err, data) {
                        console.log(data);
                        if (err) {
                            callback(true, data)
                        } else {
                            //console.log(firm + JSON.stringify(data));
                            var fecha_fin = new Date();
                            console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio));
                            if (data.status == 400) {
                                json["foto"] = {
                                    code: 403,
                                    descripcion: "No existen foto"
                                }
                                callback(false, json);
                            } else {
                                console.log(firm + data.foto.descripcion)
                                json["foto"] = {
                                    code: 200,
                                    descripcion: data.foto.descripcion
                                }
                                callback(false, json);
                            }
                        }
                    });
                }
            }
        }
    });
}

/**
 * 
 * @param {*} fuar 
 * @param {*} callback 
 */
function apiXML(fuar, base, callback) {
    const firm = "[Controller::apiXML] ";
    var fecha_inicio = new Date();
    //console.log(firm + "inicio " + fecha_inicio + " FUAR [" + fuar + "]")
    const json = {};
    var exp = "^([0-9]){13}$"
    var sql = fuar.match(exp) == null ?
        "select NOTIFICACION_ID from NOTIFICACIONESEXTRANJERO.NOTIFICACION where fuar ='" + fuar + "' " :
        "SELECT NOTIFICACION_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22)";
    //console.log(firm + sql)
    var connection = {
        user: dbConfig.base[parseInt(base)].user,
        password: dbConfig.base[parseInt(base)].password,
        connectString: dbConfig.base[parseInt(base)].connectString
    };
    DBManager.ejecute(sql, connection, function(err, result) {
        if (err) {
            json["xml"] = {
                code: 400,
                descripcion: "error conexion base de datos "
            }
            callback(true, json);
        } else {
            //let foto = result[0][1];
            if (result.length == undefined) {
                json["xml"] = {
                        code: 400,
                        descripcion: "no existe el fuar "
                    }
                    //console.log(firm + json)
                callback(false, json);
            } else {
                if (result[0] == null) {
                    //console.log(result);
                    json["xml"] = {
                        code: 400,
                        descripcion: "No existen datos"
                    }
                    var fecha_fin = new Date();
                    //console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio) + " -- falloo");
                    callback(false, json)
                } else {
                    //var sql = "select  CEPH.GET_NOTIFICACION_FUAR_NACIONAL('" + result[0][0] + "','pu') FROM DUAL";
                    var prefijo = dbConfig.base[parseInt(base)].prefijo ? ",'pu'" : '';
                    //var sql = `select  CEPH.GET_FOTO('${result[0][0]}'${prefijo}) FROM DUAL`;
                    var sql = fuar.match(exp) == null ? `select  GET_NOTIFICACION_FUAR_EXT('${result[0][0]}'${prefijo}) FROM DUAL` :
                        `select  CEPH.GET_NOTIFICACION_FUAR_NACIONAL('${result[0][0]}'${prefijo}) FROM DUAL`;
                    //console.log(firm + sql)
                    blob.run(sql, base, "xml", (fuar + ".xml"), json, function(err, data) {
                        if (err) {
                            callback(true, data)
                        } else {
                            //console.log("----------" + data.status)
                            if (data.status) {

                            } else {

                            }
                            fs.readFile(dbConfig.imagenes.direccion + "/xml-" + fuar + ".xml", 'utf8', function(err, x) {
                                if (err) {
                                    //console.log("error +++" + err);
                                    json["xml"] = {
                                        code: 400,
                                        descripcion: "No encontre el XML"
                                    }
                                    callback(false, json);
                                } else {
                                    var fecha_fin = new Date();
                                    //console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio));
                                    json["xml"] = {
                                        code: 200,
                                        descripcion: x
                                    }
                                    callback(false, json);
                                }
                            });
                        }
                    });
                }
            }
        }
    });
}


/**
 * 
 * @param {*} fuar 
 * @param {*} callback 
 */
function apiFirma(fuar, base, callback) {
    const firm = "[Controller::apiFirma] ";
    var fecha_inicio = new Date();
    console.log(firm + "inicio " + fecha_inicio + " FUAR [" + fuar + "]")
    var json = {};
    var exp = "^([0-9]){13}$";
    var sql = fuar.match(exp) == null ?
        "SELECT FIRMA_ID from NOTIFICACIONESEXTRANJERO.NOTIFICACION where fuar ='" + fuar + "' " :
        "SELECT FIRMA_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22) order by  estatusnotificacion_id ";
    //var sql = "SELECT FIRMA_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22) order by  estatusnotificacion_id "
    //console.log(firm + sql)
    var connection = {
        user: dbConfig.base[parseInt(base)].user,
        password: dbConfig.base[parseInt(base)].password,
        connectString: dbConfig.base[parseInt(base)].connectString
    };
    DBManager.ejecute(sql, connection, function(err, result) {
        if (err) {
            json["firma"] = {
                code: 400,
                descripcion: "error conexion base de datos "
            }
            callback(true, json);
        } else {
            if (result[0] == undefined) {
                json["firma"] = {
                        code: 400,
                        descripcion: "no existe el fuar "
                    }
                    //console.log(firm + json)
                callback(false, json);
            } else {
                //console.log(result.length)
                //console.log(result[0][0]);
                if (result[0][0] == null) {
                    //console.log(result);                    
                    json["firma"] = {
                        code: 400,
                        descripcion: "No existen datos "
                    }
                    var fecha_fin = new Date();
                    //console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio) + " -- falloo");
                    callback(false, json)
                } else {
                    //console.log("damosss " + result[0][0])
                    var prefijo = dbConfig.base[parseInt(base)].prefijo ? ",'pu'" : '';
                    var sql = `select  CEPH.GET_FIRMA('${result[0][0]}'${prefijo}) FROM DUAL`;
                    console.log(firm, " ", sql)
                    blob.run(sql, base, "firma", (fuar + ".jpg"), json, function(err, data) {
                        if (err) {
                            callback(true, data)
                        } else {
                            //console.log(firm + JSON.stringify(data));
                            var fecha_fin = new Date();
                            // console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio));
                            // console.log("******************" + data);
                            if (false) {

                            } else {

                                json["firma"] = {
                                    code: 200,
                                    descripcion: data.firma.descripcion
                                }
                                callback(false, json);
                            }
                        }
                    });
                }
            }
        }
    });
}

function apiHistorial(fuar, base, callback) {
    const firm = "[Controller::apiHistorial] ";
    var fecha_inicio = new Date();
    //console.log(firm + "inicio " + fecha_inicio + " FUAR [" + fuar + "]")
    const json = {};
    var sql = "SELECT sm.FUAR,sm.SOLICITUDMAC_ID FROM solicitudes.solicitudmac sm,solicitudes.tramite t, solicitudes.estatustramite et WHERE sm.fuar ='" + fuar + "' AND sm.solicitudmac_id = t.solicitudmac_id AND t.estatustramite_id = et.estatustramite_id";
    var connection = {
        user: dbConfig.base[parseInt(base)].user,
        password: dbConfig.base[parseInt(base)].password,
        connectString: dbConfig.base[parseInt(base)].connectString
    };
    DBManager.ejecute(sql, connection, function(err, result) {
        if (err) {
            json["historial"] = {
                code: 400,
                descripcion: "error conexion base de datos "
            }
        } else {

            if (result[0] == undefined) {
                //console.log(result);
                json["historial"] = {
                    code: 400,
                    descripcion: "No existen datos"
                }
                callback(false, json)
            } else {
                console.log(firm + result[0][1])
                var sql = "select ht.ESTATUSTRAMITE_ID, et.CLAVE, ht.FECHA_ESTATUS_TRAMITE from solicitudes.historicotramite ht, solicitudes.estatustramite et where ht.solicitudmac_ID in (" + result[0][1] + ") and ht.ESTATUSTRAMITE_ID = et.ESTATUSTRAMITE_ID order by historicotramite_id asc"
                DBManager.ejecute(sql, connection, function(err, result) {
                    if (err) {
                        json["historial"] = {
                            code: 400,
                            descripcion: "error conexion base de datos  "
                        }
                        callback(false, json);
                    } else {
                        json["historial"] = {
                            code: 200,
                            descripcion: result
                        }
                        var fecha_fin = new Date();
                        // console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio));
                        callback(false, json);
                    }
                });
            }
        }
    });
}

/**
 * 
 * @param {*} fuar 
 * @param {*} callback 
 */
function apiHuellas(fuar, base, callback) {
    const firm = "[Controller::apiHuellas] ";
    var fecha_inicio = new Date();
    //console.log(firm + "inicio " + fecha_inicio + " FUAR [" + fuar + "]")
    const json = {};
    var exp = "^([0-9]){13}$";
    var sql = fuar.match(exp) == null ?
        "SELECT HUELLAS_ID from NOTIFICACIONESEXTRANJERO.NOTIFICACION where fuar ='" + fuar + "' " :
        "SELECT HUELLAS_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22) order by  estatusnotificacion_id  ";
    //var sql = "SELECT HUELLAS_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22) order by  estatusnotificacion_id  "
    var connection = {
        user: dbConfig.base[parseInt(base)].user,
        password: dbConfig.base[parseInt(base)].password,
        connectString: dbConfig.base[parseInt(base)].connectString
    };
    DBManager.ejecute(sql, connection, function(err, result) {
        if (err) {
            json["huellas"] = {
                code: 400,
                descripcion: "error conexion base de datos  "
            }
        } else {
            //let foto = result[0][1];
            if (result.length == undefined) {
                json["huellas"] = {
                        code: 400,
                        menssage: "no existe el fuar "
                    }
                    //console.log(firm + json)
                callback(false, json);
            } else {
                if (result[0] == undefined) {
                    //console.log(result);
                    json["huellas"] = {
                        code: 402,
                        descripcion: "No existen datos"
                    }
                    var fecha_fin = new Date();
                    //console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio) + " -- falloo");
                    callback(false, json)
                } else {
                    json["huellas"] = {};
                    var k = 1;
                    for (let i = 1; i <= 10; i++) {
                        var prefijo = dbConfig.base[parseInt(base)].prefijo ? ",'pu'" : '';
                        var sql = `select  CEPH.GET_HUELLAS('${result[0][0]}','${i}'${prefijo}) FROM DUAL`;
                        blob.run(sql, base, "wsq", (i + "-" + fuar + ".wsq"), json, function(err, data) {
                            if (err) {
                                //console.log("fallo " + i);
                            } else {
                                //console.log("exito " + i + "  nombre: " + JSON.stringify(data))
                                var splitTemp = JSON.stringify(data.wsq.descripcion).split("/");
                                var nombre = splitTemp[splitTemp.length - 1].replace('"', '');
                                //console.log(nombre)
                                var ruta = `http://${dbConfig.wsq.ip}:${dbConfig.wsq.puerto}/huellas?url=${dbConfig.wsq.direccion}/${nombre}`;
                                //console.log(ruta)
                                request({
                                    uri: ruta,
                                    //json: true, // Para que lo decodifique automáticamente 
                                }).then(huella => {
                                    //console.log(JSON.parse(huella).status)
                                    if (JSON.parse(huella).status == 200) {
                                        //console.log("******" + huella);
                                        json["huellas"]["huella_" + i] = JSON.parse(huella);
                                    }
                                    k++;
                                    if (k == 11) {

                                        callback(false, json);
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }
    });
}

/**
 * regresa un json  con lo que contiene el archivo 
 * @param {*} fuar 
 * @param {*} callback 
 */
function apiHuellasWSQ(fuar, base, callback) {
    const firm = "[Controller::apiHuellasWSQ] ";
    var fecha_inicio = new Date();
    console.log(firm + "inicio " + fecha_inicio + " FUAR [" + fuar + "]")
    const json = {};
    var exp = "^([0-9]){13}$";
    var sql = fuar.match(exp) == null ?
        "SELECT HUELLAS_ID from NOTIFICACIONESEXTRANJERO.NOTIFICACION where fuar ='" + fuar + "' " :
        "SELECT HUELLAS_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22) order by  estatusnotificacion_id  ";
    //var sql = "SELECT HUELLAS_ID from NOTIFICACIONES.NOTIFICACION where Fuar ='" + fuar + "' and tiponotificacion_id in (21,17,24,22) order by  estatusnotificacion_id  "
    var connection = {
        user: dbConfig.base[parseInt(base)].user,
        password: dbConfig.base[parseInt(base)].password,
        connectString: dbConfig.base[parseInt(base)].connectString
    };
    DBManager.ejecute(sql, connection, function(err, result) {
        if (err) { //si tiene error con la conexion de la base de datos 
            json["huellas"] = {
                code: 400,
                descripcion: "error conexion base de datos  "
            }
        } else {
            //let foto = result[0][1];
            if (result.length == undefined) {
                json["huellas"] = {
                        code: 400,
                        menssage: "no existe el fuar "
                    }
                    //console.log(firm + json)
                callback(false, json);
            } else {
                if (result[0] == undefined) {
                    //console.log(result);
                    json["huellas"] = {
                        code: 402,
                        descripcion: "No existen datos"
                    }
                    var fecha_fin = new Date();
                    console.log(firm + "fin " + fecha_fin + " FUAR [" + fuar + "] tiempo " + (fecha_fin - fecha_inicio) + " -- falloo");
                    callback(false, json)
                } else {
                    var k = 1;
                    auxHuellas(1, base, result[0][0], fuar, {}, function(data) {
                        //console.log(data);
                        callback(false, data);
                    });

                    // for (let i = 1; i <= 10; i++) {
                    //     var sql = "select  CEPH.GET_HUELLAS('" + result[0][0] + "'," + i + ") FROM DUAL";
                    //     blob.run(sql,base, "wsq", (i + "-" + fuar + ".wsq"), json, function(err, data) {
                    //         //console.log(data.wsq)
                    //         if (err) {
                    //             console.log("fallo " + i);
                    //             json["h-" + i] = "No exixte la huella " + i;
                    //             if (k == 10) {
                    //                 console.log(json)
                    //                 callback(false, json)
                    //             }
                    //             k++;
                    //             //1909225100161
                    //         } else {
                    //             //console.log("exito " + i + "  nombre: " + JSON.stringify(data))
                    //             //console.log(firm + Object.keys(data))
                    //             //console.log("iteracion " + i + " datos: " + JSON.stringify(data.wsq));

                    //             if (data.wsq.status == 200) {
                    //                 //console.log(data.wsq.descripcion.split("/")[5]);
                    //                 fs.readFile(dbConfig.wsq.direccion + '/' + data.wsq.descripcion.split("/")[5], 'base64', function(err, x) {
                    //                     if (err) {
                    //                         console.log("error +++" + err);
                    //                         json["wsq"] = {
                    //                                 code: 400,
                    //                                 descripcion: "No encontre el XML"
                    //                             }
                    //                             // callback(false, json);
                    //                     } else {
                    //                         json["h-" + i] = {
                    //                             code: 2001,
                    //                             descripcion: x
                    //                         };

                    //                     }
                    //                 });

                    //             } else {
                    //                 json["h-" + i] = data.wsq;
                    //             }

                    //             if (k == 10) {
                    //                 callback(false, json)
                    //             }
                    //             k++;
                    //         }
                    //     });
                    // }
                }
            }
        }
    });
}


function auxHuellas(iteracion, base, huella, fuar, json, callback) {
    console.log(iteracion + "::" + huella + "::" + json);
    if (iteracion == 11) { // caso base 
        console.log("termino")
        console.log(iteracion + "::" + huella + "::" + json);
        //console.log(json)
        callback(json);
    } else {
        //json["h-" + iteracion] = iteracion;
        var resJson = {}
        var sql = "select  CEPH.GET_HUELLAS('" + huella + "'," + iteracion + ") FROM DUAL";
        blob.run(sql, base, "wsq", (iteracion + "-" + fuar + ".wsq"), resJson, function(err, data) {
            //console.log(data.wsq)
            if (err) {
                console.log("fallo " + iteracion);
                json["h-" + iteracion] = "No exixte la huella " + iteracion;
            } else {
                //console.log("exito " + iteracion + "  nombre: " + JSON.stringify(data))
                //console.log(firm + Object.keys(data))
                //console.log("iteracion " + i + " datos: " + JSON.stringify(data.wsq));
                if (data.wsq.status == 200) {
                    //console.log(data.wsq.descripcion.split("/")[5]);
                    fs.readFile(dbConfig.wsq.direccion + '/' + data.wsq.descripcion.split("/")[5], 'base64', function(err, x) {
                        if (err) {
                            console.log("error +++" + err);
                            json["wsq" + iteracion] = {
                                    code: 400,
                                    descripcion: "No encontre el WSQ"
                                }
                                // callback(false, json);
                            auxHuellas((++iteracion), base, huella, fuar, json, callback); //
                        } else {
                            json["h-" + iteracion] = {
                                code: 200,
                                descripcion: x
                            };
                            if (iteracion == 11) {
                                console.log("-----" + x)
                            }
                            auxHuellas((++iteracion), base, huella, fuar, json, callback); //
                        }
                    });

                } else {
                    json["h-" + iteracion] = data.wsq;
                    auxHuellas((++iteracion), base, huella, fuar, json, callback); //
                }

            }
        });



    }
}



//exports.getData = getData;
exports.apiHistorial = apiHistorial;
exports.apiFoto = apiFoto;
exports.apiHuellas = apiHuellas;
exports.apiFirma = apiFirma;
exports.apiHuellasWSQ = apiHuellasWSQ;
exports.apiXML = apiXML;