$(document).ready(function() {

    var conexion = 0;

    $('#fuar').on('keyup', function() {
        if ($('#fuar').val().length > 12 && $('#fuar').val().length < 15) {
            $("#enviar").prop('disabled', false);
        } else {
            $("#enviar").prop('disabled', true);
        }

    })

    $('#conexion').on('change', function() {
        conexion = $(this).val();
        ///console.log(conexion)
    });

    $("#enviar").click(function() {
        ocualtar();
        //console.log(conexion)
        var fuar = $("#fuar").val();
        if (fuar.length > 0) {

            foto(fuar, conexion, function(err, data) {
                if (err) {
                    alert(data);
                } else {
                    $(".fotoYfirma").show();
                    //console.log(Object.keys(data.foto))                    
                    if (data.foto.code == 200) {
                        $('#foto1').attr('src', data.foto.descripcion);
                        $("#foto1").show();
                    } else {
                        $('#foto1').attr('src', "public/images/dummy/sin-cara.jpeg");
                        $("#foto1").show();
                    }
                }
            });
            xml(fuar, conexion, function(err, data) {
                if (err) {
                    alert(data);
                } else {
                    //console.log("xml " + Object.keys(data.xml))
                    if (data.xml.code == 200) {
                        $("#xml").val(data.xml.descripcion);
                        $("#xml").show();
                    } else {
                        //alert("xml error " + data.xml.descripcion);
                        $("#xml").val("no existen datos");
                        $("#xml").hide();
                    }
                }
            });
            firma(fuar, conexion, function(err, data) {

                if (err) {
                    alert(data);
                } else {
                    // console.log(Object.keys(data))
                    // console.log(data.firma);
                    if (data.firma.code == 200) {
                        $('#firma1').attr('src', data.firma.descripcion);
                        $("#firma1").show();
                    } else {
                        $('#firma1').attr('src', "public/images/dummy/firma.jpg");
                        $("#firma1").show();
                    }
                }
            });

            historial(fuar, conexion, function(err, data) {
                if (err) {
                    alert(data);
                } else {
                    //console.log(data)
                    if (data.historial.code == 200) {
                        $("#table").show();
                        crearTabla(data.historial.descripcion);
                    } else {
                        alert("Hisotrial: " + data.historial.descripcion);
                    }
                }
            });

            huellas(fuar, conexion, function(err, data) {
                console.log("huellad")
                if (err) {
                    //alert("no hay huellas");
                    console.log("no encontre las huellas")
                } else {
                    //console.log("keys: " + Object.keys(data.huellas))
                    $('#barra').show();
                    if (Object.keys(data.huellas).length > 0) {
                        if (data.huellas.code == 402) {
                            //alert("Huellas  no existen");
                        } else {
                            // console.log(Object.keys(data.huellas).length);
                            // console.log(data.huellas)
                            var table_body = '<table> <thead> <tr class="table100-head"> <th class="column1" colspan="5">huellas</th>   </tr> </thead>'
                            var keys = Object.keys(data.huellas);
                            keys.sort();
                            //console.log(keys[0]);
                            for (let i = 0; i < keys.length; i++) {
                                if (i == 0) {
                                    table_body += '<tr>';
                                }
                                //console.log(keys[i]);
                                table_body += '<td>';
                                //table_body += '<td>';
                                table_body += '<h2>' + keys[i].replace("_", " ") + " </h2> <img id=j" + i + " style = 'height='150' width='150'' src=" + direccion(data.huellas[keys[i]].descripcion) + ">"
                                table_body += '</td>';
                                //table_body += '</tr>';
                                if (i == 4) {
                                    table_body += '</tr>';

                                }
                            }
                            table_body += '</table>';
                            $("#huella").show();
                            $('#tableHue').html(table_body);

                        }
                    } else {
                        //alert("no hay huellas")
                    }
                }
            });

            // huellasWSQ(fuar, conexion, function(data) {
            //     console.log(Object.keys(data))

            //     Object.keys(data).forEach(element => {
            //         if (element == "h-7") {
            //             console.log(data[element]);
            //             $("#hue2").val(data[element].descripcion);
            //         }
            //         if (element == "h-1") {
            //             //             console.log(data[element]);
            //             $("#hue1").val(data[element].descripcion);
            //         }
            //     });
            // });

        } else {
            alert("Ingresa Datos Validos");
        }

    });
});

function direccion(url) {
    var temp = url.split("/");
    var string = "";
    for (let i = 5; i < temp.length; i++) {
        string += "/" + temp[i];
    }
    //console.log(string)
    return string;
}

/**
 * 
 * @param {*} matrix 
 */
function crearTabla(tableHistorial) {
    var table_body = '<table> <thead> <tr class="table100-head"> <th class="column1" style=" width: 3%; ">Status</th> <th class="column2">Descripcion</th> <th class="column2">Fecha</th> </tr> </thead>'
    for (var i = 0; i < tableHistorial.length; i++) {
        table_body += '<tr>';
        for (var j = 0; j < tableHistorial[0].length; j++) {
            j % 3 == 0 ? table_body += '<td  >' : table_body += '<td>';
            var k = j;
            if ((++k) == tableHistorial[0].length) {
                var fecha = new Date(tableHistorial[i][j])
                var t = "hora: " + formatoHoras(fecha) +
                    " |  fecha: " + formatoFechas(fecha)
                    //console.log(fecha.getHours());
                    //console.log(t)
                table_body += t;
            } else {
                j == 1 ? table_body += tableHistorial[i][j].toLowerCase() : table_body += tableHistorial[i][j];
            }
            table_body += '</td>';
        }
        table_body += '</tr>';
    }
    table_body += '</table>';
    $('#tableDiv').html(table_body);
}

function formatoFechas(date) {
    var temp = date.toISOString().split("T")[0].split("-");

    var fecha = temp[2] + "/" + temp[1] + "/" + temp[0];
    return fecha;
}

function formatoHoras(date) {
    var temp = date.toISOString().split("T")[1].split(":");
    var fecha = temp[0] + ":" + temp[1] + ":" + temp[2].split(".")[0];
    return fecha;
}

function ocualtar() {
    $("#xml").hide();
    $("#foto1").hide();
    $("#firma1").hide();
    $("#table").hide();
    $("#huella").hide();
    $("#barra").hide();

}


/**
 * 
 * @param {*} fuar 
 */
function foto(data, conexion, callback) {
    $("#load-foto").show();
    $.ajax({
        url: "/foto",
        type: 'POST',
        dataType: 'json',
        data: {
            fuar: data,
            base: conexion
        },
        success: function(json) {
            //console.log("foto: " + JSON.stringify(json))
            $("#load-foto").hide();
            callback(false, json);
        },
        error: function(json) {
            $("#load-foto").hide();
            callback(true, json);
        }
    });
}

/**
 * 
 * @param {*} fuar 
 */
function xml(data, conexion, callback) {
    $("#load-xml").show();
    $.ajax({
        url: "/xml",
        type: 'POST',
        dataType: 'json',
        data: {
            fuar: data,
            base: conexion
        },
        success: function(json) {
            //console.log(json)
            $("#load-xml").hide();
            callback(false, json);
        },
        error: function(json) {
            $("#load-xml").hide();
            callback(true, json);
        }
    });
}

/**
 * 
 * @param {*} fuar 
 */
function firma(data, conexion, callback) {
    $("#load-firma").show();
    $.ajax({
        url: "/firma",
        type: 'POST',
        dataType: 'json',
        data: {
            fuar: data,
            base: conexion
        },
        success: function(json) {
            $("#load-firma").hide();
            callback(false, json);
        },
        error: function(json) {
            $("#load-firma").hide();
            callback(true, json);
        }
    });
}


/**
 * 
 * @param {*} fuar 
 */
function historial(data, conexion, callback) {
    $("#load-historial").show();
    $.ajax({
        url: "/hisotrial",
        type: 'POST',
        dataType: 'json',
        data: {
            fuar: data,
            base: conexion
        },
        success: function(json) {
            $("#load-historial").hide();
            callback(false, json);
        },
        error: function(json) {
            $("#load-historial").hide();
            callback(true, json);
        }
    });
}

function huellas(data, conexion, callback) {
    //$("#load-huella").show();
    $.ajax({
        url: "/huellas",
        type: 'POST',
        dataType: 'json',
        data: {
            fuar: data,
            base: conexion
        },
        success: function(json) {
            $("#load-huella").hide();
            callback(false, json);
        },
        error: function(json) {
            $("#load-huella").hide();
            callback(true, json);
        }
    });
}


/**
 * hace una peticon ajax a servidor donde 
 * regresa el conteneido de las huellas 
 * @param {*} data 
 * @param {*} callback 
 */
function huellasWSQ(data, conexion, callback) {

    $.ajax({
        url: "/wsq/huellas",
        type: "POST",
        dataType: 'json',
        data: {
            fuar: data,
            base: conexion
        },
        success: function(json) {
            //console.log("exito: " + JSON.stringify(json));
            callback(json);
        },
        error: function(json) {
            //console.log("error: " + JSON.stringify(json));
            callback(json);
        }
    });
}