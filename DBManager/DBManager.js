const oracledb = require('oracledb');
var dbConfig = require('../config/dbconfig');


var connection_ = {
    user: dbConfig.base[0].user,
    password: dbConfig.base[0].password,
    connectString: dbConfig.base[0].connectString
};



function ejecute(sql, connection, callback) {
    var mensaje = {};
    const firm = "[DBManager::ejecute] "
    console.log(firm + "" + JSON.stringify(connection))
    oracledb.getConnection(connection, function(err, cn) {
        if (err) {
            //console.log("error");
            mensaje = {
                code: 400,
                descripcion: "Error Conexion Base de Datos"
            };

            //console.log(firm + sql + mensaje)
            callback(true, mensaje);
        } else {
            cn.execute(sql, function(err, result) {
                if (err) {
                    mensaje = {
                        code: 401,
                        descripcion: "No existen datos"
                    };
                    //console.log(firm + sql + mensaje)
                    callback(false, mensaje);
                } else {
                    close(cn);

                    //console.log(firm + sql + mensaje)
                    callback(false, result.rows);

                }
            });
        }
    });
}

function close(con) {
    con.release(function(err) {
        if (err) {
            console.error("error " + err.message);
        }
    });
}


exports.ejecute = ejecute;